const cacheName = "saccman_dev";

self.addEventListener("activate", (event) => {
	event.waitUntil(
		caches.keys().then((keyList) => {
			return Promise.all(
				keyList.map((key) => {
					if (key !== cacheName && key !== "saccmanApi") {
						return caches.delete(key);
					}
				})
			);
		})
	);
});

self.addEventListener("fetch", (e) => {
	e.respondWith(
		(async () => {
			if (e.request.method == "GET") {
				if (e.request.url.includes("saccmanApi")) {
					return await fetch(e.request)
						.then(async (res) => {
							const cache = await caches.open("saccmanApi");

							cache.put(e.request, res.clone());
							return res;
						})
						.catch(async () => {
							let res = await caches.match(e.request);
							if (res === undefined) {
								return new Response();
							} else {
								return res;
							}
						});
				} else {
					const r = await caches.match(e.request);
					if (r) return r;

					const response = await fetch(e.request);
					const cache = await caches.open(cacheName);

					cache.put(e.request, response.clone());

					return response;
				}
			} else {
				return await fetch(e.request);
			}
		})()
	);
});

self.addEventListener("error", function (e) {
	console.log(e.filename, e.lineno, e.colno, e.message);
});
