export default function* dateRange(d: Date = new Date(), length = 500) {
	for (let i = 0; i < length; d.setDate(d.getDate() + 1)) {
		if (i < length) yield d.toISOString().split("T")[0];
		else return;
		i += 1;
	}
}
