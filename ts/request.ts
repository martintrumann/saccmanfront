import { Contact } from "./interfaces";
import f from "./fetch.js";
import trans from "./trans.js";

interface NewType {
	type_id: number;
	guests?: number;

	dates?: string[];

	start?: string;
	length?: number;
	end?: string;

	comment?: string;
}

interface NewRequest {
	booker: Contact;

	types?: NewType[];
	type_ids?: { [index: number]: number };

	dates?: string[];

	start?: string;
	length?: number;
	end?: string;

	comment?: string;
}

export default {
	inject: ["saccman", "lang"],
	data() {
		let req: NewRequest = {
			booker: { name: "", email: "" },
			type_ids: {},
		};

		return {
			req,
			types: [
				[1, 2],
				[2, 1],
				[3, 4],
				[4, 2],
			],
		};
	},
	methods: {
		t(str: string): string {
			return trans(str, this.lang);
		},
		makeReq(): void {
			f(this.saccman + "/request/booking", true, this.req).then(
				console.log
			);
		},
		addType(type: number, count: number): void {
			if (count > 0) {
				this.req.type_ids[type] = count;
			} else {
				delete this.req.type_ids[type];
			}
		},
	},
	template: `
	<div class="h-screen flex items-center justify-center">
		<form @submit.prevent="makeReq" class="flex flex-col">
			<div class="h-min">
				<label for="name" class="justify-self-end">{{ t("Name") }}: </label>
				<input
					id="start"
					class="border"
					type="text"
					v-model="req.booker.name"
					required
				/>

				<label for="email" class="justify-self-end">{{ t("Email") }}: </label>
				<input
					id="start"
					class="border"
					type="text"
					v-model="req.booker.email"
					required
				/>
			</div>

			<div>
				<label for="start" class="justify-self-end">{{ t("Start") }}: </label>
				<input
					id="start"
					class="border"
					type="date"
					:min="new Date().toISOString().split('T')[0]"
					v-model="req.start"
					required
				/>

				<label for="length" class="justify-self-end">{{ t("Length") }}: </label>
				<input
					id="length"
					class="border"
					type="number"
					min="1"
					max="255"
					v-model.number="req.length"
					required
				/>
			</div>

			<div>
				<div
					v-for="type in types"
				>
					{{ type[0] }}:
					<input type="number" :max="type[1]" @input="(e) => addType(type[0], Number(e.target.value))">
				</div>
			</div>

			<button class="bg-green-200">{{ t("Make Request") }}</button>
		</form>
	</div>
	`,
};
