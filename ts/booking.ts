import dateRange from "./date.js";
import t from "./trans.js";
import f from "./fetch.js";
import { BookingRoom, Room, Type } from "./interfaces";

export default {
	data() {
		return {
			error: "",
			booking: {},
			del: "",
			edit: false,
		};
	},
	inject: ["saccman", "rooms", "types", "lang"],
	props: ["bookingId"],
	emits: ["changePage", "highlightBooking"],
	methods: {
		trans(s: string): string {
			return t(s, this.lang.value);
		},
		dateRangeArr(start: string, len: number): string[] {
			if (len < 0) len = 1;
			return [...dateRange(new Date(start), len)];
		},
		newRoom(): void {
			let lastRoom = this.booking.rooms[this.booking.rooms.length - 1];

			let thisRoom =
				this.rooms.value[
					this.rooms.value.findIndex(
						(r: Room) => r.id == Number(lastRoom.id)
					) + 1
				];

			this.booking.rooms.push({
				id: thisRoom.id,
				num_of_guests:
					this.types.value[
						this.types.value.findIndex(
							(t: Type) => thisRoom.type_id == t.id
						)
					].capacity,
				dates: JSON.parse(JSON.stringify(lastRoom.dates)),
			});
		},
		del_this(): void {
			f(
				this.saccman + "/cancel/booking",
				true,
				this.booking.booking.id
			).then(() => {
				this.$emit("changePage", "table");
			});
		},
		save(): void {
			this.booking.dates = this.dateRangeArr(
				this.booking.dates[0],
				(document.querySelector("#gLength") as HTMLInputElement).value
			);

			this.booking.booking.price = Number(this.booking.booking.price);

			this.booking.rooms.forEach((i: BookingRoom) => {
				i.num_of_guests = Number(i.num_of_guests);
				i.id = Number(i.id);
			});

			f(this.saccman + "/change/booking", false, this.booking).then(
				(data) => {
					if (data == "NoSpace") {
						this.error = "noSpace";
					} else if (data === true) {
						this.$emit("changePage", "table");
					}
				}
			);
		},
		updateLength(l: number | ""): void {
			if (!!this.booking.dates[0] && !!l)
				this.booking.dates = this.dateRangeArr(
					this.booking.dates[0],
					l
				);
		},
	},
	created() {
		f(this.saccman + "/booking/" + this.bookingId)
			.then((booking) => (this.booking = booking))
			.catch((err) => {
				if (err == "DeserError") this.error = "NoConnection";
			});
	},
	template: `
	<div>
		<div
			v-if="error == 'NoConnection'"
			class="bg-red-200 border border-red-500 px-2 my-2"
		>{{ trans("Failed to connect to server") }}</div>

		<form @submit.prevent="save()" class="max-w-screen-lg m-auto px-3 md:p-0 mt-3" v-if="booking.booking">
			<div
				v-if="error == 'noSpace'"
				class="bg-red-200 border border-red-500 px-2 my-2"
			>{{ trans("No space for this booking") }}</div>
			<div class="flex justify-between items-center">
				<h2 class="text-2xl flex flex-wrap">
					<span class="mr-2">{{ trans("Booking") }} #{{ booking.booking.id }}</span>
					<span v-if="booking.booking.booking_com_id">
						({{ trans("Booking.com") }}: #{{ booking.booking.booking_com_id }})
					</span>
				</h2>

				<div
					v-if="booking.booking.is_cancled"
					class="white bg-red-500 rounded px-2 text-xl"
				>
					{{ trans("Canceled") }}
				</div>
				<button
					type="button"
					v-else-if="del == '' && !booking.booking.is_cancled"
					class="bg-red-500 rounded px-2 text-2xl"
					@click="del = 'wait'"
				>
					{{ trans("Cancel booking") }}
				</button>
				<button
					type="button"
					v-else-if="del == 'wait'"
					class="bg-red-500 rounded px-2 text-2xl"
					@click="del_this()"
				>
					{{ trans("Confirm cancellation") }}!
				</button>
			</div>
			<button
				type="button"
				class="rounded bg-blue-200 my-2 py-1 px-2"
				@click="this.$emit('highlightBooking', this.booking.booking.id)"
			>
				{{ trans("Show in table") }}
			</button>

			<div id="booker">
				<h3 class="border-b text-lg"> {{ trans("Booker") }}</h3>
				<div>{{ trans("Name") }}: {{ booking.booker.name }}</div>
				<div>{{ trans("Email") }}: {{ booking.booker.email }}</div>
				<div>{{ trans("Tel") }}: {{ booking.booker.tel }}</div>
			</div>

			<div class="flex justify-between border-b">
				<h3 class="text-lg">{{ trans("Booking Details") }}</h3>
				<button
					type="button"
					class="rounded-t bg-blue-200 px-2"
					@click="edit = !edit"
				>{{ trans("Edit") }}</button>
			</div>

			<div class="flex gap-2 items-center mb-2">
				<label for="gStart">{{ trans("Start") }}: </label>
				<input
					id="gStart"
					type="date"
					v-model="booking.dates[0]"
					:disabled="!edit"
					:class="edit? 'border' : 'border border-white bg-white'"
					required
				>

				<label for="gLength">{{ trans("Length") }}: </label>
				<input
					id="gLength"
					type="number"
					min="1"
					max="255"
					:value="booking.dates.length > 0 ? booking.dates.length : ''"
					@input="(e) => updateLength(e.target.value)"
					:disabled="!edit"
					class="w-10"
					:class="edit? 'border' : 'border border-white bg-white'"
					required
				>
			</div>

			<div class="flex gap-2 items-center mb-2">
				<label for="price">{{ trans("Price") }}: </label>
				<input
					id="price"
					type="number"
					min="0"
					max="4294967295"
					v-model="booking.booking.price"
					:disabled="!edit"
					class="w-14"
					:class="edit? 'border' : 'border border-white bg-white'"
				>

				<label for="color">{{ trans("Color") }}: </label>
				<input
					id="color"
					type="color"
					v-model="booking.booking.color"
					:disabled="!edit"
					required
				>

				<label for="is_paid">{{ trans("Is paid") }}: </label>
				<input
					id="is_paid"
					type="checkbox"
					v-model="booking.booking.is_paid"
				>
			</div>
			<div class="flex flex-col">
				<label for="comment">{{ trans("Comments") }}: </label>
				<textarea
					id="comment"
					class="ml-2 px-2"
					:class="edit ? 'border' : 'border border-white bg-white'"
					rows="5"
					v-model="booking.booking.comment"
					:disabled="!edit"
				></textarea>
			</div>

			<div id="rooms">
				<h3 class="text-lg border-b">{{ trans("Rooms") }}</h3>
				<div
					v-for="[i, room] in booking.rooms.entries()"
					:key="i"
					class="flex border-b lg:border-0 pt-1 items-center my-1"
				>
					<div class="mr-2 flex-1 flex gap-1 flex-col lg:flex-row ">
						<div>
							<label :for="'room' + i">{{ trans("Room") }}: </label>
							<input
								:id="'room' + i"
								type="number"
								min=1
								:disabled="!edit"
								v-model="room.id"
								class="w-10"
								:class="edit ? 'border' : 'border border-white bg-white'"
							/>
						</div>
						<div>
							<label :for="'guestNum' + i">{{ trans("Guests") }}: </label>
							<input
								:id="'guestNum' + i"
								type="number"
								min=1
								max=255
								v-model="room.num_of_guests"
								:disabled="!edit"
								class="w-10"
								:class="edit? 'border' : 'border border-white bg-white'"
							/>
						</div>

						<div :hidden="room.dates.length == 0">
							<label :for="'start' + i">{{ trans("Start") }}: </label>
							<input
								:id="'start' + i"
								type="date"
								:value="room.dates[0] ?? booking.dates[0]"
								@input="e => room.dates = [e.target.value]"
								:disabled="!edit"
								:class="edit ? 'border' : 'border border-white bg-white'"
							/>
						</div>
						<div :hidden="room.dates == 0">
							<label :for="'len' + i">{{ trans("Length") }}: </label>
							<input
								:id="'len' + i"
								type="number"
								:value="Number(room.dates.length)"
								min=0
								max="255"
								@input="(e) => room.dates = dateRangeArr(room.dates[0] ?? booking.dates[0], e.target.value)"
								:disabled="!edit"
								class="w-10"
								:class="edit ? 'border' : 'border border-white bg-white'"
							/>
						</div>

						<div :hidden="!edit" class="">
							<label :for="'globalDur' + i" type="checkbox">{{ trans("Use global dates") }}: </label>
							<input
								:id="'globalDur' + i"
								type="checkbox"
								:checked="room.dates.length == 0"
								@change="(e) => e.target.checked ? room.dates = [] : room.dates = booking.dates">
						</div>
					</div>
					<div v-if="i == 0 || !edit" class="w-6"></div>
					<button
						type="button"
						v-else
						class="bg-red-500 rounded w-6 h-6"
						@click="booking.rooms.splice(i, 1)"
					>X</button>
				</div>
				<div v-if="edit" class="flex justify-between">
					<button
						type="button"
						class="rounded bg-blue-200 px-3"
						@click="newRoom"
					>+</button>
					<div>
						<button
							type="submit"
							class="rounded bg-green-500 px-2 py-1"
						>{{ trans("Save") }}</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	`,
};
