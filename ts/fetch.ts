export default async function (
	url: string,
	must_be_ok = true,
	body = {}
): Promise<any | boolean> {
	if (!sessionStorage.saccmantoken) throw "No Token";

	let method = JSON.stringify(body) == "{}" ? "GET" : "POST";

	let options: RequestInit = {
		method,
		headers: {
			"Content-Type": "application/json",
			"Cache-Control": "must-revalidate",
			Authorization: sessionStorage.saccmantoken,
		},
	};

	if (method == "POST") options.body = JSON.stringify(body);

	let res = await fetch(url, options).catch(() => {
		throw "FetchError";
	});

	if (res.status == 403) {
		delete sessionStorage.saccmantoken;
		location.href = "";
	}

	if (res.headers.get("content-length") == "0") return res.ok;
	else {
		let body = await res.json().catch(() => {
			throw "DeserError";
		});

		if (!must_be_ok) return body;
		else return res.ok ? body : false;
	}
}
