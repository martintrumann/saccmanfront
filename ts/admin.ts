import trans from "./trans.js";
import f from "./fetch.js";
import { Room, Type, Price } from "./interfaces";

let PriceLine = {
	props: ["price"],
	inject: ["types", "saccman", "lang"],
	emits: ["deleted"],
	methods: {
		t(s: string) {
			return trans(s, this.lang.value);
		},
		del(p: Price) {
			if (p.id)
				f(this.saccman + "/delete/price", true, p.id).then((res) => {
					if (res) {
						delete this.price;
						this.$emit("deleted");
					}
				});
			else this.$emit("deleted");
		},
		save() {
			if (this.price.id)
				f(this.saccman + "/change/price", true, this.price).then(
					(res) => (res ? console.log("success") : "")
				);
			else
				f(this.saccman + "/add/price", true, this.price).then((res) =>
					res ? (this.price.id = res) : console.error("WRONG")
				);
		},
	},
	template: `
	<tr>
		<th scope="row" class="text-left">
			<select
				id="type"
				v-model="price.type_id"
			>
				<option v-for="type in types.value" :value="type.id">{{ t(type.name) }}</option>
			</select>
		</th>

		<td>
			<input
				type="number"
				class="border w-10"
				min="0"
				:max="types.value.find((t) => t.id == price.type_id)?.capacity"
				v-model.number="price.num_of_guests"
				@keydown.delete="delete price.num_of_guests"
				@keydown.enter="save"
			>
		</td>

		<td>
			<input
				type="date"
				class="border"
				v-model="price.beginning"
				@keydown.enter="save"
			>
		</td>

		<td>
			<input
				type="date"
				class="border"
				v-model="price.ending"
				@keydown.enter="save"
			>
		</td>

		<td>
			<input
				type="number"
				step="0.01"
				class="w-14 border"
				v-model.number="price.price"
				@keydown.enter="save"
			>
		</td>

		<td>
			<button
				class="bg-red-500 rounded px-1 font-bold mr-1"
				@click="del(price)"
			>X</button>

			<button
				class="bg-green-500 rounded px-1 mr-1"
				@click="save"
			>{{ t("Save") }}</button>
		</td>
	</tr>
	`,
};

export default {
	components: {
		PriceLine,
	},
	data() {
		return {
			prices: [],
			links: [
				["Prices", ""],
				["Types", "types"],
				["Rooms", "rooms"],
			],
			page: location.hash.split("#")[2] ?? "",
			error: "",
		};
	},
	inject: ["saccman", "lang", "types", "rooms"],
	methods: {
		t(s: string) {
			return trans(s, this.lang.value);
		},
		del(t: Type) {
			if (t.id)
				f(this.saccman + "/delete/type", false, t.id).then((data) => {
					if (data === true)
						this.types.value.splice(
							this.types.value.findIndex(
								(typ: Type) => typ.id == t.id
							),
							1
						);

					if (data === "Breaks rooms") {
						this.error = "Rooms with this type exist";
					} else if (data === "Breaks prices") {
						this.error = "Prices with this type exist";
					}
				});
			else
				this.types.value.splice(
					this.types.value.findIndex((typ: Type) => typ.id == t.id),
					1
				);
		},
		save(t: Type) {
			if (t.id) f(this.saccman + "/change/type", true, t);
			else
				f(this.saccman + "/add/type", true, t).then(
					(id) => (t.id = id)
				);
		},
		delRoom(r: Room): void {
			if (r.id)
				f(this.saccman + "/delete/room", false, r.id).then((data) => {
					if (data === true)
						this.rooms.value.splice(
							this.rooms.value.findIndex(
								(room: Room) => room.id == r.id
							),
							1
						);

					if (data === "Breaks booking_rooms")
						this.error = "Bookings with this room exist";
				});
			else
				this.rooms.value.splice(
					this.rooms.value.findIndex((room: Room) => room.id == r.id),
					1
				);
		},
		saveRoom(r: Room | any) {
			if (r.id) f(this.saccman + "/change/room", true, r);
			else {
				r.id = r.new_id;
				f(this.saccman + "/add/room", true, r).then(
					(id) => (r.id = id)
				);
			}
		},
	},
	created() {
		f(this.saccman + "/prices").then((data) => (this.prices = data));
	},
	template: `
	<div class="flex gap-5 items-start">
		<div class="flex flex-col bg-yellow-200">
			<a
				v-for="[name, link] in links"
				class="px-2 py-1 hover:bg-yellow-400"
				:href="'#admin#' + link"
				@click="page = link"
			>
				{{ t(name) }}
			</a>
		</div>

		<div class="max-w-screen-md m-auto w-full">
			<div
				v-if="error"
				class="bg-red-200 border-red-500 px-2 py-1 rounded"
			>
				<button class="px-1 font-bold bg-red-500 rounded" @click="error = ''">X</button>
				{{ t(error) }}!
			</div>

			<div v-if="page == ''" >
				<h2 class="text-xl font-bold">{{ t("Prices") }}: </h2>
				<table class="border-separate">
					<thead>
						<tr class="text-left">
							<th scope="col">{{ t("Type") }}</th>
							<th scope="col">{{ t("Guests") }}</th>
							<th scope="col">{{ t("Beginning") }}</th>
							<th scope="col">{{ t("Ending") }}</th>
							<th scope="col">{{ t("Price") }}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<price-line v-for="price in prices" :price="price" @deleted="prices.splice(prices.findIndex((p) => p.id == price.id ), 1)"></price-line>
					</tbody>
				</table>

				<button
					class="bg-blue-200 rounded px-2 py-1"
					@click="this.prices.push({})"
				>
					+
				</button>
			</div>
			<div v-else-if="page == 'types'">
				<h2 class="font-bold text-xl">{{ t("Types") }}:</h2>
				<table>
					<tr>
						<th>{{ t("Name") }}</th>
						<th>{{ t("Capacity") }}</th>
						<th>{{ t("Price") }}</th>
					</tr>
					<tr v-for="type in types.value">
						<td>
							<input
								class="border"
								type="text"
								v-model="type.name"
								@keydown.enter="save(type)"
							>
						</td>
						<td>
							<input
								class="border w-14"
								type="number"
								min="0"
								max="999"
								v-model.number="type.capacity"
								@keydown.enter="save(type)"
							>
						</td>
						<td>
							<input
								class="border w-14"
								type="number"
								min="0"
								max="999"
								v-model.number="type.default_price"
								@keydown.enter="save(type)"
							>
						</td>
						<td>
							<button
								class="bg-red-500 px-1 font-bold rounded mr-1"
								@click="del(type)"
							>
								X
							</button>

							<button
								class="bg-green-500 px-1 rounded"
								@click="save(type)"
							>
								{{ t("Save") }}
							</button>
						</td>
					</tr>
				</table>
				<button
					class="bg-blue-200 rounded px-2 py-1"
					@click="this.types.value.push({})"
				>
					+
				</button>
			</div>
			<div v-else-if="page = 'rooms'">
				<h2 class="font-bold text-xl">{{ t("Rooms") }}:</h2>
				<table class="border-separate">
					<tr>
						<th>{{ t("id") }}</th>
						<th>{{ t("Name") }}</th>
						<th>{{ t("Type") }}</th>
					</tr>

					<tr v-for="room in rooms.value">
						<td v-if="room.id">
							{{ room.id }}
						</td>
						<td v-else>
							<input
								class="border w-14"
								type="number"
								min="1"
								v-model.number="room.new_id"
							>
						</td>

						<td>
							<input
								class="border"
								type="text"
								v-model="room.name"
								keydown.delete="delete room.name"
							>
						</td>

						<td>
							<select v-model="room.type_id">
								<option v-for="type in types.value" :key="type.id" :value="type.id">{{ t(type.name) }}</option>
							</select>
						</td>

						<td>
							<button
								class="bg-red-500 px-1 font-bold rounded mr-1"
								@click="delRoom(room)"
							>
								X
							</button>

							<button
								class="bg-green-500 px-1 rounded"
								@click="saveRoom(room)"
							>
								{{ t("Save") }}
							</button>
						</td>
					</tr>
				</table>
				<button
					class="bg-blue-200 rounded px-2 py-1"
					@click="this.rooms.value.push({})"
				>
					+
				</button>
			</div>
		</div>
	</div>
	`,
};
