export default {
	props: ["name", "value", "label"],
	template: `
	<div class="">
		<label :for="name">{{ label }}:</label>
		<input class="border border-gray-700 rounded" v-bind="$attrs" :id="name" :type="name == 'password' ? 'password' : 'text'" :value="value" >
	</div>
	`,
};
