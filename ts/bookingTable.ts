import dateRange from "./date.js";
import fetch from "./fetch.js";
import { BookingRoom, Room, Type } from "./interfaces";
import trans from "./trans.js";

export default {
	data() {
		return {
			bookings: {},
			loading: false,
		};
	},
	inject: ["saccman", "lang", "rooms", "types"],
	props: {
		start: String,
		end: String,
		length: Number,
		highlight: Number,
		noTotal: Boolean,
	},
	emits: ["openBooking", "newBooking"],
	methods: {
		t(s: string): string {
			return trans(s, this.lang.value);
		},
		newDateRange(start: string, length: number) {
			return dateRange(new Date(start), length);
		},
		getCell(
			bookings: BookingRoom[],
			room: Room
		): [number, number, BookingRoom] {
			let booking: BookingRoom;
			if (bookings)
				booking = bookings.filter(
					(b: BookingRoom) => b.room_id == room.id
				)[0];

			let type = this.types.value.find((t: Type) => t.id == room.type_id);

			return [room.id, type.capacity, booking];
		},
		format(date: string): string {
			const spl = date.split("-");
			return spl[2] + "." + spl[1] + "." + spl[0].substr(2);
		},
		total(date: string): number {
			if (this.bookings[date])
				return this.bookings[date].reduce(
					(a: number, i: BookingRoom) => a + i.num_of_guests,
					0
				);
		},
		update() {
			let end: string;
			if (this.length) {
				let d = new Date(this.start);
				d.setDate(d.getDate() + this.length - 1);
				end = d.toISOString().split("T")[0];
			} else if (this.end) end = this.end;
			else throw "No end";

			this.loading = true;
			fetch(this.saccman + "/bookings/" + this.start + "/" + end).then(
				(data) => {
					this.bookings = data;
					this.loading = false;
				}
			);
		},
		getId(room: [Room, Type]): number {
			return room[0].id;
		},
		getGuestNum(br: BookingRoom, capacity: number): string {
			let gn = br.num_of_guests;

			if (gn <= capacity) return gn.toString();
			else return capacity.toString() + "+" + (gn - capacity);
		},
		getTextColor(c: string): string {
			return Math.round(
				(parseInt(c.substring(1, 3), 16) * 299 +
					parseInt(c.substring(3, 5), 16) * 587 +
					parseInt(c.substring(5, 7), 16) * 114) /
					1000
			) > 125
				? "text-black"
				: "text-white";
		},
	},
	watch: {
		start() {
			if (this.start) this.update();
		},
		end() {
			if (this.end) this.update();
		},
		length() {
			if (this.length > 0) this.update();
		},
	},
	created() {
		this.update();
	},
	template: `
	<table v-if="types" class="max-w-full m-auto transition" :class="loading ? 'opacity-50' : ''">
		<thead>
			<tr>
				<th class="z-20 sticky left-0 top-0 bg-white cursor-pointer" @click="update">
					&#8635;
				</th>
				<th class="z-10 sticky top-0 bg-white" v-if="!noTotal">{{ t("Total") }}</th>
				<th v-for="room in rooms.value" class="sticky top-0 bg-white">{{ room.id }}</th>
			</tr>
		</thead>
		<tbody>
			<tr v-for='date in newDateRange(start, length)' :class="total(date) > 0 ? '' : 'bg-gray-100'" >
				<td class="border-b pr-2 sticky left-0 bg-white">{{ format(date) }}</td>
				<td class="border h-7 p-0 font-bold text-center" v-if="!noTotal">{{ total(date) }}</td>
				<td v-for="[room, capacity, booking] in rooms.value.map(r => getCell(bookings[date], r))"
					style="min-width: 2rem"
					class="w-8 sm:w-16 border h-7 p-0"
				>
					<a v-if="booking"
						:href="'#booking' + booking.booking_id"
						:style="'background-color:' + booking.color"
						class="w-fill h-full text-right flex items-center justify-end"
						:class="
							highlight == booking.booking_id ? 'border-2 border-black' : '',
							getTextColor(booking.color)
						"
						@click="$emit('openBooking', booking.booking_id)"
					>
						{{ getGuestNum(booking, capacity) }}
					</a>
					<div
						v-else
						class="
							f-full
							h-full
							cursor-pointer
							opacity-0
							hover:opacity-100
							flex
							items-center
							justify-center
							text-green-600
							font-bold
							text-lg
						"
						@click="$emit('newBooking', date, room)"
					>+</div>
				</td>
			</tr>
		</tbody>
	</table>
	`,
};
