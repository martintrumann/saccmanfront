let t = {
	// General
	Booking: { ee: "Broneering" },
	Guests: { ee: "Külalised" },
	Price: { ee: "Hind" },
	Save: { ee: "Salvesta" },
	Back: { ee: "Tagasi" },

	Type: { ee: "Tüüp" },
	Types: { ee: "Tüübid" },

	Capacity: { ee: "Maht" },

	Date: { ee: "Kuupäev" },

	// Nav
	Table: { en: "Bookings Timetable", ee: "Broneeringute ajatabel" },
	"Booking List": { ee: "Broneeringute nimekiri" },
	"New Booking": { ee: "Uus broneering" },
	"Log out": { ee: "Välju süsteemist" },

	// Login
	Username: { ee: "Kasutajanimi" },
	Password: { ee: "Parool" },
	Login: { ee: "Sisene süsteemi" },
	"Wrong password": { ee: "Vale parool" },
	"No user found": { ee: "Kasutajat ei leitud" },
	"Something went wrong": { ee: "Midagi läks valesti" },
	"Login Failed, Try Again": {
		ee: "Sisselogimine ebaõnnestus, proovi uuesti",
	},

	// Table
	Total: { ee: "Kokku" },

	// Booking Forms
	Booker: { ee: "Broneerija" },
	Name: { ee: "Nimi" },
	Email: { ee: "Meil" },
	Start: { ee: "Algus" },
	Dates: { ee: "Kuupäevad" },
	Length: { ee: "Pikkus" },
	Duration: { ee: "Pikkus" },
	Add: { ee: "Lisa" },

	Color: { ee: "Värv" },
	"Is paid": { ee: "Makstud" },

	Automatic: { ee: "Automaatne" },

	Room: { ee: "Tuba" },
	Rooms: { ee: "Toad" },
	"Guests Number": { ee: "Külaliste number" },
	"Booking Details": { ee: "Broneeringu detailid" },
	Confirm: { ee: "Kinnita" },
	"Cancel booking": { ee: "Broneeringu tühistamine" },
	"Confirm cancellation": { ee: "Kinnita tühistamine" },
	Edit: { ee: "Muuda" },
	Cancel: { ee: "Tühista" },
	Canceled: { ee: "Tühistatud" },
	"Use global dates": {
		en: "Use booking dates",
		ee: "Kasuta broneeringu kuupäevi",
	},
	Comments: { ee: "Märkused" },

	"Something Went Wrong": { ee: "Midagi läks valesti" },
	"Enter either email or tel": { ee: "Sisesta meil või tel" },
	"Enter either email, tel or Booking.com id": {
		ee: "Sisesta meil, telefon või Booking.com id",
	},
	"No space for this booking": { ee: "Broneeringu jaoks pole ruumi" },
	"Enter Rooms": { ee: "Sisesta tube" },

	"Show in table": { ee: "Näita tabelis" },
	"Calculate Price": { ee: "Arvuta hind" },
	// List
	"Show canceled": { ee: "Näita tühistatud" },
	"Show past": { ee: "Näita vanu" },
	Past: { ee: "Vana" },

	// Admin
	Prices: { ee: "Hinnad" },
	Beginning: { ee: "Algus" },
	Ending: { ee: "Lõpp" },
	"Rooms with this type exist": { ee: "Sellel tüübil on tube" },
	"Prices with this type exist": { ee: "Sellel tüübil on hindu" },
	"Bookings with this room exist": { ee: "Sellel toal on broneeringuid" },
};

export default function (s: string, lang: string): string {
	return (t[s] && t[s][lang]) ?? s;
}
