import t from "./trans.js";
import f from "./fetch.js";
import { SmallBooking } from "./interfaces";

interface Filters {
	show_canceled: boolean;
	show_past: boolean;
	name: string;
	id?: number;
	date?: string;
}

export default {
	inject: ["saccman", "lang"],
	emits: ["openBooking"],
	data() {
		let filters: Filters = {
			show_canceled: false,
			show_past: false,
			name: "",
		};

		return {
			bookings: {},
			filters,
			loading: true,
		};
	},
	methods: {
		trans(s: string): string {
			return t(s, this.lang.value);
		},
		getBookings(): SmallBooking[] {
			return this.bookings;
		},
		getDates(dates: string[]): string {
			let start = dates[0];
			return (
				start +
				(dates.length > 1 ? " - " + dates[dates.length - 1] : "")
			);
		},
		inPast(b: SmallBooking): boolean {
			return (
				new Date(b.dates[b.dates.length - 1]) <
				new Date(Date.now() - 86400000)
			);
		},
		shouldShow(b: SmallBooking): boolean {
			let pastFilter = this.filters.show_past
				? true
				: new Date(b.dates[b.dates.length - 1]) >
				  new Date(Date.now() - 86400000);

			let canceledFilter = this.filters.show_canceled
				? true
				: !b.booking.is_cancled;

			let IdFilter = this.filters.id
				? String(b.booking.id).includes(this.filters.id) ||
				  String(b.booking.booking_com_id).includes(this.filters.id)
				: true;

			let nameFilter = b.booker.name.includes(this.filters.name);

			let dateFilter = this.filters.date
				? b.dates.includes(this.filters.date)
				: true;

			let final =
				pastFilter &&
				canceledFilter &&
				IdFilter &&
				dateFilter &&
				nameFilter;

			return final;
		},
	},
	watch: {
		"filters.date"(f: string) {
			this.filters.show_past = new Date(f) < new Date();
		},
		"filters.id"(f: string) {
			this.filters.show_past = !!f;
		},
		"filters.name"(f: string) {
			this.filters.show_past = !!f;
		},
	},
	created() {
		f(this.saccman + "/bookings/all").then((data) => {
			this.bookings = data;
			this.loading = false;
		});
	},
	template: `
	<div class="m-auto flex gap-2 flex-col md:flex-row px-3 md:px-0">
		<div class="
			pt-3
			grid
			grid-cols-2
			self-start
			items-center
			sticky
			top-0
			bg-white
			w-full
			md:w-auto
		">
				<label for="show_past">{{ trans("Show past") }}:</label>
				<div>
					<input
						id="show_past"
						type="checkbox"
						v-model="filters.show_past"
					>
				</div>

				<label for="show_canceled">{{ trans("Show canceled") }}:</label>
				<input
					id="show_canceled"
					type="checkbox"
					v-model="filters.show_canceled"
				>

				<label for="filter_name">{{ trans("Name") }}: </label>
				<input
					id="filter_name"
					type="text"
					v-model="filters.name"
					class="border"
				>

				<label for="filter_id">{{ trans("Number") }}: </label>
				<input
					id="filter_id"
					type="number"
					v-model.number="filters.id"
					class="border"
				>

				<label for="filter_date">{{ trans("Date") }}: </label>
				<input id="filter_date" type="date" v-model="filters.date" @keydown.delete="delete filters.date">
		</div>

		<div class="mt-3 max-w-screen-md flex-1 md:mx-auto">
			<div v-if="loading" class="mx-auto w-min">{{ trans("Loading") }}...</div>
			<a
				v-for="booking in getBookings()"
				:key="booking.booking.id"
				:href="'#booking' + booking.booking.id"
				@click="$emit('openBooking', booking.booking.id)"
				:class="shouldShow(booking) ? 'flex' : 'hidden'"
				style="border-left-width: 10px;"
				:style="'border-left-color:' + booking.booking.color"
				class=" border rounded mb-2 p-2 flex justify-between items-center gap-2 "
			>
				<div class="flex-1">
					<h2 class="inline text-lx font-bold">
						<span v-if="booking.booking.booking_com_id">B #{{ booking.booking.booking_com_id }}: </span>
						<span v-else>#{{ booking.booking.id }}: </span>
						{{ booking.booker.name }}
					</h2>
					<div>{{ getDates(booking.dates) }} </div>
				</div>

				<span v-if="booking.booking.is_cancled" class="bg-red-400 px-2 py-1 rounded">{{ trans("Canceled") }}</span>
				<span v-if="inPast(booking)" class="bg-blue-400 px-2 py-1 rounded">{{ trans("Past") }}</span>
			</a>
		</div>

	</div>
	`,
};
