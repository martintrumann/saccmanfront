import NewBooking from "./form.js";
import BookingTable from "./bookingTable.js";
import BookingsList from "./bookingList.js";
import RequestList from "./requestList.js";
import trans from "./trans.js";
import fInput from "./f-input.js";
import Booking from "./booking.js";
import LoginForm from "./login.js";
import AdminPage from "./admin.js";
import RequestForm from "./request.js";
import f from "./fetch.js";
import { Room, Type } from "./interfaces";

let saccman = "/saccmanApi";

// @ts-ignore
//prod Vue.createApp({
/*dev*/ window.vue = Vue.createApp({
	components: {
		fInput,
		LoginForm,
		NewBooking,
		BookingTable,
		BookingsList,
		Booking,
		AdminPage,
		RequestForm,
		RequestList,
	},
	data() {
		return {
			token: sessionStorage.saccmantoken ?? "",
			page: window.location.hash.substring(1),
			rooms: [] as Room[],
			types: [] as Type[],
			lang: "ee",
			error: "",
			highlight: null,
			power: 0,
			navSmall: true,
		};
	},
	provide() {
		return {
			saccman: saccman,
			// @ts-ignore
			lang: Vue.computed(() => this.lang),
			// @ts-ignore
			rooms: Vue.computed(() => this.rooms),
			// @ts-ignore
			types: Vue.computed(() => this.types),
		};
	},
	watch: {
		page(p: string) {
			window.location.hash = p;
			this.navSmall = true;
		},
	},
	methods: {
		t(str: string) {
			return trans(str, this.lang);
		},
		logout() {
			sessionStorage.saccmantoken = "";
			this.token = "";
		},
		login(token: string) {
			this.token = token;

			f(saccman + "/rooms").then((data: Room[]) => (this.rooms = data));
			f(saccman + "/types").then((data: Type[]) => (this.types = data));
		},
		openBooking(booking: number): void {
			this.page = "booking" + booking;
		},
		newBooking(date: string, room: number): void {
			this.page = `newBookingR${room}D${date}`;
		},
		hl(b: number) {
			this.page = "";
			this.highlight = b;

			window.setTimeout(() => (this.highlight = null), 5000);
		},
	},
	created() {
		if (sessionStorage.saccmantoken) {
			fetch(saccman + "/check", {
				headers: {
					Authorization: sessionStorage.saccmantoken,
					"Cache-Control": "no-store",
				},
			}).then((res) => {
				if (!res.ok) {
					sessionStorage.saccmantoken = "";
					this.token = "";
				} else {
					res.json().then((power: number) => (this.power = power));

					f(saccman + "/types").then(
						(data: Type[]) => (this.types = data)
					);
					f(saccman + "/rooms").then((data: Room[]) => {
						this.rooms = data.sort((a, b) => {
							if (
								[2, 3].includes(a.type_id) &&
								[2, 3].includes(b.type_id)
							)
								return 0;

							return a.type_id - b.type_id;
						});
					});
				}
			});
		}
	},
}).mount("#vue");

if ("serviceWorker" in navigator) {
	navigator.serviceWorker.register("./sw.js");
}
