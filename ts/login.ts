import trans from "./trans.js";

async function login(url: string, name: string, password: string) {
	let res = await fetch(url, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Cache-Control": "no-store",
		},
		body: JSON.stringify({ name, password }),
	});

	return [res.ok, await res.json()];
}

export default {
	data() {
		return { loading: false, error: "" };
	},
	inject: ["saccman", "lang"],
	emits: ["changePage", "changeLang", "regToken"],
	methods: {
		t(s: string) {
			return trans(s, this.lang.value);
		},
		login(e: Event) {
			if (!(e.target instanceof HTMLFormElement)) {
				return false;
			}

			let body = { name: "", password: "" };
			e.target
				.querySelectorAll("input")
				.forEach((i: HTMLInputElement) => {
					body[i.id] = i.value;
				});

			if (body.name && body.password) {
				login(this.saccman + "/login", body.name, body.password).then(
					([ok, json]) => {
						this.loading = false;
						if (ok) {
							let token = json;
							sessionStorage.saccmantoken = token;
							this.$emit("regToken", token);
							this.$emit("changePage", "");
						} else {
							this.error = json;
						}
					}
				);
				this.loading = true;
			}
		},
	},
	template: `
	<div
		class="h-screen flex justify-center items-center"
	>
		<span v-if="this.loading">{{ t("Loading") }}...</span>
		<form
			v-else
			@submit.prevent="login"
			class="border rounded p-2 grid gap-2"
		>
			<img
				src="./img/logo_v2.png"
				alt=""
				class="col-span-2 h-20 m-auto"
			/>

			<div class="col-span-2 text-center" v-if="error">
				{{ t(error) }}!
			</div>

			<label for="name"> {{ t("Name") }}: </label>
			<input
				id="name"
				type="text"
				class="border border-gray-700 rounded"
				required
			/>

			<label for="password"> {{ t("Password") }}: </label>
			<input
				id="password"
				type="password"
				class="border border-gray-700 rounded"
				required
			/>

			<button
				type="submit"
				class="
					py-2
					px-4
					bg-blue-100
					hover:bg-blue-300
					col-span-2
				"
			>
				{{ t("Login") }}
			</button>
		</form>
	</div>
	`,
};
