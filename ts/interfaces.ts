export interface BookingRoom {
	id: number;
	num_of_guests: number;
	booking_id: number;
	room_id: number;
}

export interface Booking {
	id: number;
	added: string;
	is_cancled: boolean;
	booker_id: number;
	booking_com_id?: number;
	price?: number;
	color: string;
	comment?: string;
}

export interface Room {
	id: number;
	name?: string;
	type_id: number;
}

export interface Type {
	id: number;
	name: string;
	capacity: number;
	default_price: number;
}

export interface Contact {
	name: string;
	email?: string;
	tel?: string;
}

export interface SmallBooking {
	booking: Booking;
	booker: Contact;
	dates: string[];
}

export interface Price {
	id: number;
}
