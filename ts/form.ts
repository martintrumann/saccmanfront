import t from "./trans.js";
import f from "./fetch.js";
import { Contact } from "./interfaces";
import BookingTable from "./bookingTable.js";

interface NewBooking {
	booker: Contact;

	booking_com_id?: number;
	color: string;

	price?: number;

	rooms?: NewRoom[];
	room_ids?: number[];
	guests_num?: number;

	dates?: string[];
	start?: string;
	length?: number;
	end?: string;
}

interface NewRoom {
	room_id: number;
	guests_num?: number;

	dates?: string[];
	start?: string;

	length?: number;

	end?: string;
}
let colors = ["#fcb75d", "#f1fc5d", "#fc685d", "#b75dfc", "#5dfcb7", "#fcef5d"];

export default {
	data() {
		let room = location.hash.match(/R(\d+)/);

		let start = location.hash.match(/D(\d{4}-\d{2}-\d{2})/);

		let today = new Date().toISOString().split("T")[0];

		let booking: NewBooking = {
			booker: { name: "" },
			room_ids: room ? [Number(room[1])] : [],
			rooms: [],
			start: start ? start[1] : today,
			length: 1,
			color: colors[Math.floor(Math.random() * 6)],
		};

		return { autoPrice: true, error: "", booking, today };
	},
	components: { BookingTable },
	props: ["start"],
	emits: ["changePage"],
	inject: ["rooms", "saccman", "lang"],
	methods: {
		trans(s: string) {
			return t(s, this.lang.value);
		},
		newBooking() {
			if (
				!this.booking.booker.email &&
				!this.booking.booker.tel &&
				!this.booking.booking_com_id
			) {
				this.error = "noContact";

				return false;
			}

			if (
				this.booking.room_ids.length == 0 &&
				this.booking.rooms.length == 0
			)
				this.error = "noGuests";

			if (!this.booking.price) {
				this.calcPrice();
				return false;
			}

			f(this.saccman + "/new/booking", false, this.booking).then(
				(data) => {
					if (data == "NoSpace") {
						this.error = "noSpace";
					} else if (data) {
						this.$emit("changePage", "table");
					} else {
						this.error = "somethingWrong";
					}
				}
			);
		},
		toggleRoom(room: number) {
			if (this.booking.rooms.some((r: NewRoom) => r.room_id == room))
				this.booking.rooms.splice(
					this.booking.rooms.findIndex(
						(r: NewRoom) => r.room_id == room
					),
					1
				);
			else {
				let index = this.booking.room_ids.indexOf(room);

				if (index >= 0) this.booking.room_ids.splice(index, 1);
				else this.booking.room_ids.push(room);
			}
		},
		delIfEmpty(val: string) {
			if (this.booking[val] == "") {
				delete this.booking[val];
			}
		},
		calcPrice() {
			if (!this.autoPrice) return;
			let priceReq = {
				start: this.booking.start,
				length: this.booking.length,
				rooms: this.booking.rooms.map((r: NewRoom) => {
					return [r.room_id, r.guests_num];
				}),
				room_ids: this.booking.room_ids,
			};

			f(this.saccman + "/price", true, priceReq).then((data) => {
				if (data) this.booking.price = data;
				else delete this.booking.price;
			});
		},
		hasRoom(room: number) {
			let in_rooms = this.booking.rooms.some(
				(r: NewRoom) => r.room_id == room
			);
			return this.booking.room_ids.includes(room) || in_rooms;
		},
		addGuests(room: number, e: InputEvent) {
			if (e.target instanceof HTMLInputElement) {
				if (this.booking.room_ids.includes(room)) {
					let index = this.booking.room_ids.indexOf(room);
					this.booking.room_ids.splice(index, 1);

					this.booking.rooms.push({
						room_id: room,
						guests_num: Number(e.target.value),
					});
				} else if (e.target.value == "") {
					let i = this.booking.rooms.findIndex((r: NewRoom) => {
						return r.room_id == room;
					});

					this.booking.rooms.splice(i, 1);
					this.booking.room_ids.push(room);
				} else {
					this.booking.rooms.find((r: NewRoom) => {
						return r.room_id == room;
					}).guests_num = Number(e.target.value);
				}
			}
		},
	},
	template: `
	<form
		id="newBooking"
		class="w-50 flex flex-col w-screen max-w-screen-md m-auto p-3 md:p-0 md:mt-3"
		@submit.prevent="newBooking"
	>
		<div
			v-if="error == 'somethingWrong'"
			class="bg-red-300 border border-red-500 rounded px-2"
		>
			{{ trans("Something went wrong") }}!
		</div>
		<div
			v-else-if="error == 'noContact'"
			class="bg-red-300 border border-red-500 rounded px-2"
		>
			<button
				@click="error = ''"
				class="border border-red-500 bg-red-400 rounded px-1 m-1"
			>X</button>
			{{ trans("Enter either email, tel or Booking.com id") }}!
		</div>
		<div
			v-else-if="error == 'noGuests'"
			class="bg-red-300 border border-red-500 rounded px-2"
		>
			{{ trans("Enter rooms") }}!
		</div>
		<div
			v-else-if="error == 'noSpace'"
			class="bg-red-300 border border-red-500 rounded px-2"
		>
			{{ trans("No space for this booking") }}!
		</div>
		<div id="booker" class="grid gap-2" style="grid-template-columns: min-content auto;">
			<h2 class="text-lg font-bold col-span-2 border-b">{{ trans("Booker") }}</h2>

			<label for="name" class="justify-self-end">{{ trans("Name") }}:</label>
			<input
				id="name"
				class="border flex-grow"
				type="text"
				required
				v-model="booking.booker.name"
			/>
			<label for="email" class="justify-self-end">{{ trans("Email") }}:</label>
			<input
				id="email"
				class="border flex-grow"
				type="email"
				:class="error == 'noContact' ? 'border-red-500' : ''"
				v-model="booking.booker.email"
			/>
			<label for="tel" class="justify-self-end">{{ trans("Tel") }}:</label>
			<input
				id="tel"
				class="border flex-grow"
				type="text"
				:class="error == 'noContact' ? 'border-red-500' : ''"
				v-model="booking.booker.tel"
			/>
		</div>
		<div id="booking" class="mt-2">
			<h2 class="text-lg font-bold col-span-2 border-b">{{ trans("Booking Details") }}</h2>
			<div class="flex grid gap-2 mt-2 items-center" style="grid-template-columns: min-content auto;">
				<label
					for="booking_com_id"
					class="justify-self-end whitespace-nowrap"
				>
					{{ trans("Booking.com ID") }}:
				</label>
				<input
					id="booking_com_id"
					class="border flex-1"
					:class="error == 'noContact' ? 'border-red-500' : ''"
					type="number"
					min="0"
					max="18446744073709551615"
					v-model.number="booking.booking_com_id"
					@keyup.delete="delIfEmpty('booking_com_id')"
				/>

				<label for="price" class="justify-self-end">{{ trans("Price") }}: </label>
				<div class="flex items-center gap-2 flex-col sm:flex-row">
					<input
						id="price"
						type="number"
						min="0"
						max="4294967295"
						step="0.01"
						class="flex-1 border"
						v-model.number="booking.price"
						@focus="calcPrice"
					>
					<div class="flex items-center">
						<label for="autoPrice"> {{ trans("Automatic") }}: </label>
						<input id="autoPrice" type="checkbox" v-model="autoPrice">
					</div>
				</div>

				<label
					for="color"
					class="justify-self-end whitespace-nowrap"
				>
					{{ trans("Color") }}:
				</label>
				<input
					id="color"
					class="border flex-1"
					type="color"
					v-model.number="booking.color"
				/>

			</div>
			<div class="flex flex-col">
				<label for="comment">{{ trans("Comments") }}: </label>
				<textarea
					id="comment"
					v-model="booking.comment"
					class="border"
					rows="5"
				></textarea>
			</div>

			<h2 class="text-lg font-bold col-span-2 border-b">{{ trans("Dates") }}</h2>

			<div class="flex grid gap-2 mt-2 items-center" style="grid-template-columns: min-content auto;">
				<label for="start" class="justify-self-end">{{ trans("Start") }}: </label>
				<input
					id="start"
					class="border"
					type="date"
					:min="today"
					v-model="booking.start"
					required
				/>

				<label for="length" class="justify-self-end">{{ trans("Length") }}: </label>
				<input
					id="length"
					class="border"
					type="number"
					min="1"
					max="255"
					v-model.number="booking.length"
					required
				/>

				<label for="rooms" class="justify-self-end">{{ trans("Rooms") }}: </label>
				<div
					id="rooms"
					class="flex overflow-auto"
					:class="error == 'noGuests' ? 'border border-red-500' : ''"
				>
					<div
						v-for="room in rooms.value.map((r) => r.id)"
						class="grid w-10 items-baseline"
						style="height: 50px;"
					>
						<button
							type="button"
							:value="room"
							class="px-2 cursor-pointer"
							:class="hasRoom(room) ? 'bg-blue-400': ''"
							@click="toggleRoom(room)"
						>
							{{ room }}
						</button>

						<input
							v-if="hasRoom(room)"
							type="number"
							min="1"
							max="99"
							class="border w-full"
							@input="(e) => addGuests(room, e)"
						>
					</div>
				</div>

			</div>
		</div>
		<button
			type="submit"
			class="mt-2 p-2 px-3 text-white rounded-b-lg"
			:class="booking.price === undefined ? 'bg-yellow-600' : 'bg-blue-600'"
		>
			{{ booking.price === undefined ? trans("Calculate Price") : trans("Add")  }}
		</button>

		<div class="max-h-80 overflow-auto mt-2">
			<booking-table
				:start="booking.start"
				:length="booking.length > 0 ? booking.length : 1"
				noTotal
			></booking-table>
		</div>
	</form>
	`,
};
