import f from "./fetch.js";

export default {
	data() {
		return {
			requests: {},
		};
	},
	inject: ["saccman", "lang"],
	methods: {
		getDates(dates: string[]): string {
			let start = dates[0];
			return (
				start +
				(dates.length > 1 ? " - " + dates[dates.length - 1] : "")
			);
		},
	},
	created() {
		f(
			this.saccman +
				"/requests/" +
				new Date().toISOString().split("T")[0] +
				"/" +
				new Date(Date.now() + 2592000).toISOString().split("T")[0]
		).then((data) => (this.requests = data));
	},
	template: `
	<div class="mt-3">
			<a
				v-for="request in requests"
				:key="request.booking.id"
				:href="'#booking' + request.booking.id"
				@click="$emit('openBooking', request.booking.id)"
				class=" border rounded mb-2 p-2 flex justify-between items-center gap-2 "
			>
				<div class="flex-1">
					<h2 class="inline text-lx font-bold">
						#{{ request.booking.id }}: {{ request.booker.name }}
					</h2>
					<div>{{ getDates(request.dates) }} </div>
				</div>

				<span v-if="request.booking.is_cancled" class="bg-red-400 px-2 py-1 rounded">{{ trans("Canceled") }}</span>
			</a>
	</div>
	`,
};
